import { DynamicModule, Inject, Module } from '@nestjs/common';

@Module({})
class D {
  constructor() {
    console.debug('D initialized')
  }

  static register(value): DynamicModule {
    return {
      module: D,
      providers: [
        {
          provide: 'foo',
          // useValue: value, // this works (i.e., creates one instance for each 'value')
          // useValue: () => value, // this doesn't works
          useFactory: () => value, // this doesn't work
        },
      ],
      exports: ['foo'],
    }
  }
}

@Module({
  imports: [
    D.register('a'),
  ],
})
class A {
  constructor(@Inject("foo") readonly foo) {
    console.debug('A initialized with foo =', foo)
  }
}

@Module({
  imports: [
    D.register('b'),
  ],
})
class B {
  constructor(@Inject("foo") readonly foo) {
    console.debug('B initialized with foo =', foo)
  }
}

@Module({
  imports: [
    A,
    B,
  ],
})
export class AppModule {}
